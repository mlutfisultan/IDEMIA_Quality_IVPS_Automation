package pages;

import com.shaft.gui.element.ElementActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class F6_Skipped_Violations_Monitoring_Page {
    public By Skipped_Violation_Page_Btn = By.id("pages-entry-skipped-violations-monitoring");
    public By Date_From = By.cssSelector("#form-control-startDate-input .p-inputtext");
    public By Date_To = By.cssSelector("#form-control-endDate-input .p-inputtext");
    public By Searching_By_Username = By.id("form-control-user-input");
    public By Searching_By_Radar_Code = By.id("form-control-radarCode-input");
    public By Submit_Btn = By.id("dynamic-form-submit-form-button");
    public By Search_Count = By.cssSelector(".p-datatable-tfoot td:nth-child(2)");
    public By Display_All_Btn = By.id("dynamic-table-action-col-0");
    public By Return_Vio_Btn = By.id("dynamic-table-action-col-row0-button1");
    public By Confirmation_of_Return_By_Yes_Btn = By.id("skipped-violations-monitoring-dialog-yes");
    public By Confirmation_of_Return_By_No_Btn = By.id("skipped-violations-monitoring-dialog-no");
    WebDriver driver;

    public F6_Skipped_Violations_Monitoring_Page(WebDriver driver) {
        this.driver = driver;
    }

    public void Select_Skipped_Violation_Page_Btn() {
        ElementActions.click(driver, Skipped_Violation_Page_Btn);
    }

    public void Type_Skipped_Violation_From_Date() {
        ElementActions.type(driver, Date_From, "2022/03/01");
    }

    public void Type_Skipped_Violation_To_Date() {
        ElementActions.type(driver, Date_To, "2022/03/31");
    }

    public void Type_Skipped_Violation_Username() {
        ElementActions.type(driver, Searching_By_Username, "Violation Test Account");
    }

    public void Type_Skipped_Violation_Radar_Code() {
        ElementActions.type(driver, Searching_By_Radar_Code, "EGY005");
    }

    public void Select_Skipped_Violation_Submit_Btn() {
        ElementActions.click(driver, Submit_Btn);
    }

    public void Get_Skipped_Violation_Search_Count() {
        ElementActions.getElementsCount(driver, Search_Count);
    }

    public void Select_Skipped_Violation_Display_Vio_Btn() {
        ElementActions.click(driver, Display_All_Btn);
    }

    public void Select_Skipped_Violation_Return_Vio_Btn() {
        ElementActions.click(driver, Return_Vio_Btn);
    }

    public void Select_Skipped_Violation_Confirm_Return_Yes_Btn() {
        ElementActions.click(driver, Confirmation_of_Return_By_Yes_Btn);
    }

    public void Select_Skipped_Violation_Confirm_Return_No_Btn() {
        ElementActions.click(driver, Confirmation_of_Return_By_No_Btn);
    }
}
