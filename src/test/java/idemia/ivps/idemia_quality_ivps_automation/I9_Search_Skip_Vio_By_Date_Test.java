package idemia.ivps.idemia_quality_ivps_automation;

import com.shaft.driver.DriverFactory;
import com.shaft.gui.browser.BrowserActions;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.A1_Main_Page;
import pages.B2_Welcome_Page;
import pages.F6_Skipped_Violations_Monitoring_Page;

public class I9_Search_Skip_Vio_By_Date_Test {
    private WebDriver driver;
    private A1_Main_Page MainPage_Obj;
    private B2_Welcome_Page WelcomePage_Obj;
    private F6_Skipped_Violations_Monitoring_Page SkippedViolationsMonitoringPage_Obj;

    @BeforeClass
    public void setUp() {
        driver = DriverFactory.getDriver();
        driver.get("http://cqcehap001.qcenv.idemia.local:8000/vps-hub/login");
        MainPage_Obj = new A1_Main_Page(driver);
        WelcomePage_Obj = new B2_Welcome_Page(driver);
        SkippedViolationsMonitoringPage_Obj = new F6_Skipped_Violations_Monitoring_Page(driver);
    }

    @AfterClass
    public void tearDown() {
        WelcomePage_Obj.IVPS_Logout();
        BrowserActions.closeCurrentWindow(driver);
    }

    @Test(description = "Login to IVPS", priority = 0)
    @Severity(SeverityLevel.BLOCKER)
    public void IVPS_Login() {
        MainPage_Obj.IVPS_SignIn();
        String Welcome_Page_Label = driver.findElement(WelcomePage_Obj.Welcome_Page_Label).getText();
        System.out.println("Page Label is " + Welcome_Page_Label);
    }

    @Test(description = "Search for Skipped Violations List By Date", priority = 1)
    @Severity(SeverityLevel.CRITICAL)
    public void Search_for_Skipped_Violations_List() {
        WelcomePage_Obj.Select_Violation_Management_Btn();
        SkippedViolationsMonitoringPage_Obj.Select_Skipped_Violation_Page_Btn();
        SkippedViolationsMonitoringPage_Obj.Type_Skipped_Violation_From_Date();
        SkippedViolationsMonitoringPage_Obj.Type_Skipped_Violation_To_Date();
        SkippedViolationsMonitoringPage_Obj.Select_Skipped_Violation_Submit_Btn();
        SkippedViolationsMonitoringPage_Obj.Get_Skipped_Violation_Search_Count();
        String Skipped_Violation_Search_Count = driver.findElement(SkippedViolationsMonitoringPage_Obj.Search_Count).getText();
        System.out.println(" Skipped Violation Search Count is " + Skipped_Violation_Search_Count);
    }


}
