package pages;

import com.shaft.gui.element.ElementActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class D4_Search_In_Violations_Page {
    public By Search_Violations_Page_Btn = By.id("pages-entry-violations-manager");
    public By Page_Label = By.cssSelector("#breadcrumb-violations-manager > span");
    public By Search_By_Plate_Number = By.id("form-control-plateNumber-input");
    public By Search_By_Code = By.id("form-control-violationCode-input");
    public By Search_By_Username = By.id("form-control-userName-input");
    public By Search_By_Status_List = By.cssSelector("#form-control-status-input .p-multiselect-label-container");
    public By Status_1_Form_125_created = By.cssSelector("p-multiselectitem:nth-child(1)  > .p-multiselect-item");
    public By Status_2_Vio_Skipped = By.cssSelector("p-multiselectitem:nth-child(2)  > .p-multiselect-item");
    public By Status_3_Vio_Sent_to_PPO = By.cssSelector("p-multiselectitem:nth-child(3)  > .p-multiselect-item");
    public By Search_Btn = By.id("dynamic-form-submit-form-button");
    public By Search_Result = By.cssSelector(".p-datatable-tfoot td:nth-child(2)");
    public By Return_Vio_Btn = By.id("violations-manager-reset-violation-button-0");
    public By Edit_Vio_Btn = By.id("violations-manager-regenerate-violation-button-0");
    public By Vio_Data_Btn = By.id("violations-manager-viewForm-button-0");
    public By Vio_Date_Btn = By.id("violations-manager-history-violation-button-0");
    public By Closure_Btn = By.cssSelector(".p-dialog-header-close-icon");

    WebDriver driver;

    public D4_Search_In_Violations_Page(WebDriver driver) {
        this.driver = driver;
    }

    public void Select_Search_Violations_Btn() {
        ElementActions.click(driver, Search_Violations_Page_Btn);
    }

    public void Type_Search_For_Violations_Plate_Number_New() {
        ElementActions.type(driver, Search_By_Plate_Number, O15_Plate_Number_Generator.getNewPlateNumber());
    }

    public void Type_Search_For_Violations_Plate_Number_Old() {
        ElementActions.type(driver, Search_By_Plate_Number, O15_Plate_Number_Generator.getOldPlateNumber());
    }

    public void Type_Search_For_Violations_Code() {
        ElementActions.type(driver, Search_By_Code, "EGY001.040620111703.33360");
    }

    public void Type_Search_For_Violations_By_Username() {
        ElementActions.type(driver, Search_By_Username, "super");
    }

    public void Select_Search_For_Violations_By_Status() {
        ElementActions.click(driver, Search_By_Status_List);
    }

    public void Select_Status_1_Form_125_created() {
        ElementActions.click(driver, Status_1_Form_125_created);
    }

    public void Select_Status_2_Vio_Skipped() {
        ElementActions.click(driver, Status_2_Vio_Skipped);
    }

    public void Select_Status_3_Vio_Sent_to_PPO() {
        ElementActions.click(driver, Status_3_Vio_Sent_to_PPO);
    }

    public void Select_Search_For_Violations_Search_Btn() {
        ElementActions.click(driver, Search_Btn);
    }

    public void Get_Search_For_Violations_Search_Result() {
        ElementActions.getElementsCount(driver, Search_Result);
    }

    public void Select_Search_For_Violations_Return_Vio_Btn() {
        ElementActions.click(driver, Return_Vio_Btn);
    }

    public void Select_Search_For_Violations_Edit_Vio_Btn() {
        ElementActions.click(driver, Edit_Vio_Btn);
    }

    public void Select_Search_For_Violations_Vio_Data_Btn() {
        ElementActions.click(driver, Vio_Data_Btn);
    }

    public void Select_Search_For_Violations_Vio_Date_Btn() {
        ElementActions.click(driver, Vio_Date_Btn);
    }

    public void Select_Closure_Btn() {
        ElementActions.click(driver, Closure_Btn);
    }
}
