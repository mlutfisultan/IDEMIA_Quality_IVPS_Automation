package pages;

import java.util.Random;

public class O15_Plate_Number_Generator {

    private static String[] newPlateNumber;
    private static String[] oldPlateNumber;

    private static void generatePlateNumber() {
        if (newPlateNumber == null || newPlateNumber.length == 0) {
            newPlateNumber = new String[]{getRandomCharacter(), getRandomCharacter(), getRandomCharacter(), getRandomNumber()};
        }
    }

    public static String getPlateNumberByIndex(int index) {
        generatePlateNumber();

        if (index < 0 || index > 3) {
            return "";
        }
        return newPlateNumber[index];
    }

    public static String[] getPlateNumberArray() {
        generatePlateNumber();
        return newPlateNumber;
    }

    public static String getNewPlateNumber() {
        generatePlateNumber();
        return String.join(" ", newPlateNumber);
    }


    private static void generateOldPlateNumber() {
        if (oldPlateNumber == null || oldPlateNumber.length == 0) {
            oldPlateNumber = new String[]{getRandomNumber()};
        }
    }

    public static String getOldPlateNumberByIndex(int index) {
        generateOldPlateNumber();

        if (index < 0 || index > 3) {
            return "";
        }
        return oldPlateNumber[index];
    }

    public static String[] getOldPlateNumberArray() {
        generateOldPlateNumber();
        return oldPlateNumber;
    }

    public static String getOldPlateNumber() {
        generateOldPlateNumber();
        return String.join("", oldPlateNumber);
    }


    private static String getRandomCharacter() {
        Random random = new Random();
        return ((Character) (char) random.ints(1575, 1610).findAny().getAsInt()).toString();
    }

    private static String getRandomNumber() {
        Random random = new Random();
        return random.ints(100, 9999).findAny().getAsInt() + "";
    }
}
