package idemia.ivps.idemia_quality_ivps_automation;

import com.shaft.driver.DriverFactory;
import com.shaft.gui.browser.BrowserActions;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.A1_Main_Page;
import pages.B2_Welcome_Page;
import pages.J10_System_User_Management_Page;

public class L12_Users_Add_and_Search_Test {
    private WebDriver driver;
    private A1_Main_Page MainPage_Obj;
    private B2_Welcome_Page WelcomePage_Obj;
    private J10_System_User_Management_Page UserManagementPage_Obj;

    @BeforeClass
    public void setUp() {
        driver = DriverFactory.getDriver();
        driver.get("http://cqcehap001.qcenv.idemia.local:8000/vps-hub/login");
        MainPage_Obj = new A1_Main_Page(driver);
        WelcomePage_Obj = new B2_Welcome_Page(driver);
        UserManagementPage_Obj = new J10_System_User_Management_Page(driver);
    }

    @AfterClass
    public void tearDown() {
        WelcomePage_Obj.IVPS_Logout();
        BrowserActions.closeCurrentWindow(driver);
    }

    @Test(description = "Login to IVPS", priority = 0)
    @Severity(SeverityLevel.BLOCKER)
    public void IVPS_Login() {
        MainPage_Obj.IVPS_SignIn();
        String Welcome_Page_Label = driver.findElement(WelcomePage_Obj.Welcome_Page_Label).getText();
        System.out.println("Page Label is " + Welcome_Page_Label);
    }

    @Test(description = "Add New User with All Permission", priority = 1)
    @Severity(SeverityLevel.CRITICAL)
    public void Add_New_User_with_All_Permissions() {
        WelcomePage_Obj.Select_System_User_Management_Btn();
        UserManagementPage_Obj.Select_User_Manage_Add_New_User_Btn();
        UserManagementPage_Obj.Type_User_Manage_New_User_Full_Name();
        UserManagementPage_Obj.Select_User_Manage_New_User_Permission_List();
        UserManagementPage_Obj.Select_User_Manage_New_User_Permission_All();
        UserManagementPage_Obj.Type_User_Manage_New_User_Name();
        UserManagementPage_Obj.Type_User_Manage_New_Password();
        UserManagementPage_Obj.Select_User_Manage_New_User_Add_Btn();
    }

    @Test(description = "Search for New User Added Recently", priority = 2)
    @Severity(SeverityLevel.NORMAL)
    public void Search_for_New_User_Added_Recently() {
        UserManagementPage_Obj.Type_User_Manage_Search_by_Username();
        UserManagementPage_Obj.Select_User_Manage_Search_Btn();
        UserManagementPage_Obj.Wait_Invisibility_Of_Page_Spinner();
        UserManagementPage_Obj.Get_User_Manage_Search_Count();
        String User_Manage_Search_Count = driver.findElement(UserManagementPage_Obj.User_Manage_Search_Count).getText();
        System.out.println(" User Manage Search Count is " + User_Manage_Search_Count);
    }

    @Test(description = "Display All Users", priority = 3)
    @Severity(SeverityLevel.MINOR)
    public void Display_All_Users() {
        UserManagementPage_Obj.Select_User_Manage_Display_All_Btn();
        UserManagementPage_Obj.Select_User_Manage_Search_Btn();
        UserManagementPage_Obj.Wait_Invisibility_Of_Page_Spinner();
        UserManagementPage_Obj.Get_User_Manage_Search_Count();
        String User_Manage_Search_Count = driver.findElement(UserManagementPage_Obj.User_Manage_Search_Count).getText();
        System.out.println(" User Manage Search Count is " + User_Manage_Search_Count);
    }
}
