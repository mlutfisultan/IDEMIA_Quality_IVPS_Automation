package idemia.ivps.idemia_quality_ivps_automation;

import com.shaft.driver.DriverFactory;
import com.shaft.gui.browser.BrowserActions;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.A1_Main_Page;
import pages.B2_Welcome_Page;
import pages.E5_Auto_Create_Model_125_Page;

public class H8_Auto_Create_125_Test {
    private WebDriver driver;
    private A1_Main_Page MainPage_Obj;
    private B2_Welcome_Page WelcomePage_Obj;
    private E5_Auto_Create_Model_125_Page AutoCreateModel125Page_Obj;

    @BeforeClass
    public void setUp() {
        driver = DriverFactory.getDriver();
        driver.get("http://cqcehap001.qcenv.idemia.local:8000/vps-hub/login");
        MainPage_Obj = new A1_Main_Page(driver);
        WelcomePage_Obj = new B2_Welcome_Page(driver);
        AutoCreateModel125Page_Obj = new E5_Auto_Create_Model_125_Page(driver);
    }

    @AfterClass
    public void tearDown() {
        WelcomePage_Obj.IVPS_Logout();
        BrowserActions.closeCurrentWindow(driver);
    }

    @Test(description = "Login to IVPS", priority = 0)
    @Severity(SeverityLevel.BLOCKER)
    public void IVPS_Login() {
        MainPage_Obj.IVPS_SignIn();
        String Welcome_Page_Label = driver.findElement(WelcomePage_Obj.Welcome_Page_Label).getText();
        System.out.println("Page Label is " + Welcome_Page_Label);
    }

    @Test(description = "Create and Submit Bulk Of 125 Auto Generation Form", priority = 1)
    @Severity(SeverityLevel.CRITICAL)
    public void Create_and_Submit_125_Form() {
        WelcomePage_Obj.Select_Violation_Management_Btn();
        AutoCreateModel125Page_Obj.Select_Auto_Create_125_Model_Page_Btn();
        AutoCreateModel125Page_Obj.Select_Auto_Create_125_Model_Create_Forms_Automatically_Btn();
        AutoCreateModel125Page_Obj.Type_Auto_Create_125_From_Date();
        AutoCreateModel125Page_Obj.Type_Auto_Create_125_To_Date();
        AutoCreateModel125Page_Obj.Select_Auto_Create_125_Radar_List();
        AutoCreateModel125Page_Obj.Select_Auto_Create_125_Radar_List_child_EGY004();
        AutoCreateModel125Page_Obj.Get_Search_Of_Models_Count();
        String Get_Auto_Create_125_NumOfModels_Count = driver.findElement(AutoCreateModel125Page_Obj.Search_Of_Models_Count).getText();
        System.out.println("Search result = " + Get_Auto_Create_125_NumOfModels_Count);
        AutoCreateModel125Page_Obj.Select_Auto_Create_125_Form_Yes_Btn();
    }

    @Test(description = "Display Current Submitted Bulk Of 125 Auto Generation Form", priority = 2)
    @Severity(SeverityLevel.NORMAL)
    public void Display_Current_Submitted_125_Form() {
        AutoCreateModel125Page_Obj.Select_Auto_Create_125_Model_Current_Forms_Btn();
        AutoCreateModel125Page_Obj.Get_Auto_Create_125_Model_Total_Forms_Count();
        String Get_Auto_Create_125_Model_Total_Forms_Count = driver.findElement(AutoCreateModel125Page_Obj.Total_of_Forms_Count).getText();
        System.out.println("Search result = " + Get_Auto_Create_125_Model_Total_Forms_Count);
    }

    @Test(description = "Display Old Submitted Bulk Of 125 Auto Generation Form", priority = 3)
    @Severity(SeverityLevel.MINOR)
    public void Display_Old_Submitted_125_Form() {
        AutoCreateModel125Page_Obj.Select_Auto_Create_125_Model_Old_Forms_Btn();
        AutoCreateModel125Page_Obj.Get_Auto_Create_125_Model_Total_Forms_Count();
        String Get_Auto_Create_125_Model_Total_Forms_Count = driver.findElement(AutoCreateModel125Page_Obj.Total_of_Forms_Count).getText();
        System.out.println("Search result = " + Get_Auto_Create_125_Model_Total_Forms_Count);
    }
}