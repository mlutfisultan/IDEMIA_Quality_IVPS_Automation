package idemia.ivps.idemia_quality_ivps_automation;

import com.shaft.driver.DriverFactory;
import com.shaft.gui.browser.BrowserActions;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.A1_Main_Page;
import pages.B2_Welcome_Page;
import pages.I9_Radars_Page;

public class G7_Radar_Add_And_Search_Test {
    private WebDriver driver;
    private A1_Main_Page MainPage_Obj;
    private B2_Welcome_Page WelcomePage_Obj;
    private I9_Radars_Page RadarsPage_Obj;

    @BeforeClass
    public void setUp() {
        driver = DriverFactory.getDriver();
        driver.get("http://cqcehap001.qcenv.idemia.local:8000/vps-hub/login");
        MainPage_Obj = new A1_Main_Page(driver);
        WelcomePage_Obj = new B2_Welcome_Page(driver);
        RadarsPage_Obj = new I9_Radars_Page(driver);
    }

    @AfterClass
    public void tearDown() {
        WelcomePage_Obj.IVPS_Logout();
        BrowserActions.closeCurrentWindow(driver);
    }

    @Test(description = "Login to IVPS", priority = 0)
    @Severity(SeverityLevel.BLOCKER)
    public void IVPS_Login() {
        MainPage_Obj.IVPS_SignIn();
        String Welcome_Page_Label = driver.findElement(WelcomePage_Obj.Welcome_Page_Label).getText();
        System.out.println("Page Label is " + Welcome_Page_Label);
    }

    @Test(description = "Create New Function Radar", priority = 1)
    @Severity(SeverityLevel.CRITICAL)
    public void Create_New_Function_Radar() throws InterruptedException {
        WelcomePage_Obj.Select_Radars_Page_Btn();
        RadarsPage_Obj.Wait_Invisibility_Of_Page_Spinner();
        RadarsPage_Obj.Select_Radar_Add_New_Radar_Btn();
        RadarsPage_Obj.Type_Radar_Add_New_Radar_Code_Name();
        RadarsPage_Obj.Select_Radar_Add_New_Radar_Maker_List();
        RadarsPage_Obj.Select_Radar_Add_New_Radar_Maker_IDEMIA();
        RadarsPage_Obj.Select_Radar_Add_New_Radar_Model();
        RadarsPage_Obj.Select_Radar_Add_New_Radar_Model_1();
        RadarsPage_Obj.Type_Radar_Add_New_Radar_Location();
        RadarsPage_Obj.Type_Radar_Add_New_Radar_Longitude();
        RadarsPage_Obj.Type_Radar_Add_New_Radar_Latitude();
        RadarsPage_Obj.Type_Radar_Add_New_Radar_Address();
        RadarsPage_Obj.Select_Radar_Add_New_Radar_Add_Btn();
        RadarsPage_Obj.Wait_Invisibility_Of_Page_Spinner();
    }

    @Test(description = "Search For Function Radar Added Recently", priority = 2)
    @Severity(SeverityLevel.NORMAL)
    public void Search_For_Function_Radar_Added_Recently() {
        RadarsPage_Obj.Type_Radar_Add_New_Search_By_Radar_Code();
        RadarsPage_Obj.Select_Radar_Add_New_Search_Btn();
        RadarsPage_Obj.Wait_Invisibility_Of_Page_Spinner();
        RadarsPage_Obj.Get_Radar_Add_New_Search_Count();
        String Get_Radar_Add_New_Search_Count = driver.findElement(RadarsPage_Obj.Search_Count).getText();
        System.out.println("Search result = " + Get_Radar_Add_New_Search_Count);
    }

    @Test(description = "Create New Compact Radar", priority = 3)
    @Severity(SeverityLevel.CRITICAL)
    public void Create_New_Compact_Radar() throws InterruptedException {
        RadarsPage_Obj.Select_Radar_Add_New_Radar_Btn();
        RadarsPage_Obj.Type_Radar_Add_New_Radar_Code_Name();
        RadarsPage_Obj.Select_Radar_Add_New_Radar_Maker_List();
        RadarsPage_Obj.Select_Radar_Add_New_Radar_Maker_IDEMIA();
        RadarsPage_Obj.Select_Radar_Add_New_Radar_Model();
        RadarsPage_Obj.Select_Radar_Add_New_Radar_Model_2();
        RadarsPage_Obj.Type_Radar_Add_New_Radar_Location();
        RadarsPage_Obj.Type_Radar_Add_New_Radar_Longitude();
        RadarsPage_Obj.Type_Radar_Add_New_Radar_Latitude();
        RadarsPage_Obj.Type_Radar_Add_New_Radar_Address();
        RadarsPage_Obj.Select_Radar_Add_New_Radar_Add_Btn();
        RadarsPage_Obj.Wait_Invisibility_Of_Page_Spinner();
    }

    @Test(description = "Search For Compact Radar Added Recently", priority = 4)
    @Severity(SeverityLevel.NORMAL)
    public void Search_For_Compact_Radar_Added_Recently() {
        RadarsPage_Obj.Type_Radar_Add_New_Search_By_Radar_Code();
        RadarsPage_Obj.Select_Radar_Add_New_Search_Btn();
        RadarsPage_Obj.Wait_Invisibility_Of_Page_Spinner();
        RadarsPage_Obj.Get_Radar_Add_New_Search_Count();
        String Get_Radar_Add_New_Search_Count = driver.findElement(RadarsPage_Obj.Search_Count).getText();
        System.out.println("Search result = " + Get_Radar_Add_New_Search_Count);
    }

    @Test(description = "Create New Mobile Radar", priority = 5)
    @Severity(SeverityLevel.CRITICAL)
    public void Create_New_Mobile_Radar() throws InterruptedException {
        RadarsPage_Obj.Select_Radar_Add_New_Radar_Btn();
        RadarsPage_Obj.Type_Radar_Add_New_Radar_Code_Name();
        RadarsPage_Obj.Select_Radar_Add_New_Radar_Maker_List();
        RadarsPage_Obj.Select_Radar_Add_New_Radar_Maker_IDEMIA();
        RadarsPage_Obj.Select_Radar_Add_New_Radar_Model();
        RadarsPage_Obj.Select_Radar_Add_New_Radar_Model_3();
        RadarsPage_Obj.Type_Radar_Add_New_Radar_Location();
        RadarsPage_Obj.Type_Radar_Add_New_Radar_Longitude();
        RadarsPage_Obj.Type_Radar_Add_New_Radar_Latitude();
        RadarsPage_Obj.Type_Radar_Add_New_Radar_Address();
        RadarsPage_Obj.Select_Radar_Add_New_Radar_Add_Btn();
        RadarsPage_Obj.Wait_Invisibility_Of_Page_Spinner();
    }

    @Test(description = "Search For Mobile Radar Added Recently", priority = 6)
    @Severity(SeverityLevel.NORMAL)
    public void Search_For_Mobile_Radar_Added_Recently() {
        RadarsPage_Obj.Type_Radar_Add_New_Search_By_Radar_Code();
        RadarsPage_Obj.Select_Radar_Add_New_Search_Btn();
        RadarsPage_Obj.Wait_Invisibility_Of_Page_Spinner();
        RadarsPage_Obj.Get_Radar_Add_New_Search_Count();
        String Get_Radar_Add_New_Search_Count = driver.findElement(RadarsPage_Obj.Search_Count).getText();
        System.out.println("Search result = " + Get_Radar_Add_New_Search_Count);
    }

    @Test(description = "Create New Flow Radar", priority = 7)
    @Severity(SeverityLevel.CRITICAL)
    public void Create_New_Flow_Radar() throws InterruptedException {
        RadarsPage_Obj.Select_Radar_Add_New_Radar_Btn();
        RadarsPage_Obj.Type_Radar_Add_New_Radar_Code_Name();
        RadarsPage_Obj.Select_Radar_Add_New_Radar_Maker_List();
        RadarsPage_Obj.Select_Radar_Add_New_Radar_Maker_IDEMIA();
        RadarsPage_Obj.Select_Radar_Add_New_Radar_Model();
        RadarsPage_Obj.Select_Radar_Add_New_Radar_Model_4();
        RadarsPage_Obj.Type_Radar_Add_New_Radar_Location();
        RadarsPage_Obj.Type_Radar_Add_New_Radar_Longitude();
        RadarsPage_Obj.Type_Radar_Add_New_Radar_Latitude();
        RadarsPage_Obj.Type_Radar_Add_New_Radar_Address();
        RadarsPage_Obj.Select_Radar_Add_New_Radar_Add_Btn();
        RadarsPage_Obj.Wait_Invisibility_Of_Page_Spinner();
    }

    @Test(description = "Search For Flow Radar Added Recently", priority = 8)
    @Severity(SeverityLevel.NORMAL)
    public void Search_For_Flow_Radar_Added_Recently() {
        RadarsPage_Obj.Type_Radar_Add_New_Search_By_Radar_Code();
        RadarsPage_Obj.Select_Radar_Add_New_Search_Btn();
        RadarsPage_Obj.Wait_Invisibility_Of_Page_Spinner();
        RadarsPage_Obj.Get_Radar_Add_New_Search_Count();
        String Get_Radar_Add_New_Search_Count = driver.findElement(RadarsPage_Obj.Search_Count).getText();
        System.out.println("Search result = " + Get_Radar_Add_New_Search_Count);
    }

    @Test(description = "Display List of Radars", priority = 9)
    @Severity(SeverityLevel.MINOR)
    public void Display_List_Of_Radars() {
        RadarsPage_Obj.Select_Radar_Add_New_Display_All_Btn();
        RadarsPage_Obj.Wait_Invisibility_Of_Page_Spinner();
        RadarsPage_Obj.Get_Radar_Add_New_Search_Count();
        String Get_Radar_Add_New_Search_Count = driver.findElement(RadarsPage_Obj.Search_Count).getText();
        System.out.println("Search result = " + Get_Radar_Add_New_Search_Count);
    }
}
