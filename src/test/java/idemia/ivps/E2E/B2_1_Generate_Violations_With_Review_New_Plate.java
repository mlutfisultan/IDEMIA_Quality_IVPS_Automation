package idemia.ivps.E2E;

import com.shaft.driver.DriverFactory;
import com.shaft.gui.browser.BrowserActions;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.A1_Main_Page;
import pages.B2_Welcome_Page;
import pages.C3_Form_125_Page;

public class B2_1_Generate_Violations_With_Review_New_Plate {
    private WebDriver driver;
    private A1_Main_Page MainPage_Obj;
    private B2_Welcome_Page WelcomePage_Obj;
    private C3_Form_125_Page Form125Page_Obj;

    @BeforeClass
    public void setUp() {
        driver = DriverFactory.getDriver();
        driver.get("http://cqcehap001.qcenv.idemia.local:8000/vps-hub/login");
        MainPage_Obj = new A1_Main_Page(driver);
        WelcomePage_Obj = new B2_Welcome_Page(driver);
        Form125Page_Obj = new C3_Form_125_Page(driver);
    }

    @AfterClass
    public void tearDown() {
        WelcomePage_Obj.IVPS_Logout();
        BrowserActions.closeCurrentWindow(driver);
    }

    @Test(description = "Login to IVPS", priority = 0)
    @Severity(SeverityLevel.BLOCKER)
    public void IVPS_Login() {
        MainPage_Obj.IVPS_SignIn();
        String Welcome_Page_Label = driver.findElement(WelcomePage_Obj.Welcome_Page_Label).getText();
        System.out.println("Page Label is " + Welcome_Page_Label);
    }

    @Test(description = "Create and Submit 125 Form for New Plate ID", priority = 1)
    @Severity(SeverityLevel.CRITICAL)
    public void Create_and_Submit_125_Form_using_New_Plate_ID() {
        Form125Page_Obj.Select_Form_125_Generator_Btn();
        Form125Page_Obj.Select_Form_125_Arrangement_Type();
        Form125Page_Obj.Select_Form_125_Arrangement_Type_Dsc();
        Form125Page_Obj.Type_Form_125_Violation_From_Date();
        Form125Page_Obj.Select_Form_125_Review_Activation();
        Form125Page_Obj.Select_Form_125_Review_Activation_Yes();
        Form125Page_Obj.Select_Form_125_View_Violations_Btn();
        Form125Page_Obj.Type_Form_125_Plate_ID();
        Form125Page_Obj.Select_Form_125_Type_of_Offense();
        Form125Page_Obj.Select_List_Of_Form_125_Type_Offense_Child();
        Form125Page_Obj.Select_Offenses_List_closer_Btn();
        Form125Page_Obj.Select_Form_125_Creation_Btn();
        Form125Page_Obj.Select_Form_125_Generation_Btn();
        Form125Page_Obj.Wait_Invisibility_Of_Page_Spinner();
        WelcomePage_Obj.Select_Main_Menu_Btn();
    }
}
