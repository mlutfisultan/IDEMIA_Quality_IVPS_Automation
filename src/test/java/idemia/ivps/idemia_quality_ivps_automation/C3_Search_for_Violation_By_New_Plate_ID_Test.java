package idemia.ivps.idemia_quality_ivps_automation;

import com.shaft.driver.DriverFactory;
import com.shaft.gui.browser.BrowserActions;
import com.shaft.gui.element.ElementActions;
import com.shaft.validation.Assertions;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.A1_Main_Page;
import pages.B2_Welcome_Page;
import pages.D4_Search_In_Violations_Page;

public class C3_Search_for_Violation_By_New_Plate_ID_Test {
    private WebDriver driver;
    private A1_Main_Page MainPage_Obj;
    private B2_Welcome_Page WelcomePage_Obj;
    private D4_Search_In_Violations_Page SearchInViolationsPage_Obj;

    @BeforeClass
    public void setUp() {
        driver = DriverFactory.getDriver();
        driver.get("http://cqcehap001.qcenv.idemia.local:8000/vps-hub/login");
        MainPage_Obj = new A1_Main_Page(driver);
        WelcomePage_Obj = new B2_Welcome_Page(driver);
        SearchInViolationsPage_Obj = new D4_Search_In_Violations_Page(driver);
    }

    @AfterClass
    public void tearDown() {
        WelcomePage_Obj.IVPS_Logout();
        BrowserActions.closeCurrentWindow(driver);
    }


    @Test(description = "Login to IVPS", priority = 0)
    @Severity(SeverityLevel.BLOCKER)
    public void IVPS_Login() {
        MainPage_Obj.IVPS_SignIn();
        String Welcome_Page_Label = driver.findElement(WelcomePage_Obj.Welcome_Page_Label).getText();
        System.out.println("Page Label is " + Welcome_Page_Label);
    }

    @Test(description = "Search of Violations for New Plate ID", priority = 1)
    @Severity(SeverityLevel.NORMAL)
    public void Search_For_Violations_With_New_Plate() {
        WelcomePage_Obj.Select_Violation_Management_Btn();
        SearchInViolationsPage_Obj.Select_Search_Violations_Btn();
        Assertions.assertTrue(driver.findElement(SearchInViolationsPage_Obj.Page_Label).isDisplayed());
        SearchInViolationsPage_Obj.Type_Search_For_Violations_Plate_Number_New();
        SearchInViolationsPage_Obj.Select_Search_For_Violations_Search_Btn();
        ElementActions.getElementsCount(driver, SearchInViolationsPage_Obj.Search_Result);
        String Search_For_Violations_Search_Result = driver.findElement(SearchInViolationsPage_Obj.Search_Result).getText();
        System.out.println("Search result = " + Search_For_Violations_Search_Result);
    }
}
