package idemia.ivps.E2E;

import com.shaft.driver.DriverFactory;
import com.shaft.gui.browser.BrowserActions;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.A1_Main_Page;
import pages.B2_Welcome_Page;
import pages.C3_Form_125_Page;
import pages.D4_Search_In_Violations_Page;

public class C3_1_Regenerate_Violations_using_Update_New_Plate {
    private WebDriver driver;
    private A1_Main_Page MainPage_Obj;
    private B2_Welcome_Page WelcomePage_Obj;
    private C3_Form_125_Page Form125Page_Obj;
    private D4_Search_In_Violations_Page SearchInViolationsPage_Obj;

    @BeforeClass
    public void setUp() {
        driver = DriverFactory.getDriver();
        driver.get("http://cqcehap001.qcenv.idemia.local:8000/vps-hub/login");
        MainPage_Obj = new A1_Main_Page(driver);
        WelcomePage_Obj = new B2_Welcome_Page(driver);
        Form125Page_Obj = new C3_Form_125_Page(driver);
        SearchInViolationsPage_Obj = new D4_Search_In_Violations_Page(driver);

    }

    @AfterClass
    public void tearDown() {
        WelcomePage_Obj.IVPS_Logout();
        BrowserActions.closeCurrentWindow(driver);
    }

    @Test(description = "Login to IVPS", priority = 0)
    @Severity(SeverityLevel.BLOCKER)
    public void IVPS_Login() {
        MainPage_Obj.IVPS_SignIn();
        String Welcome_Page_Label = driver.findElement(WelcomePage_Obj.Welcome_Page_Label).getText();
        System.out.println("Page Label is " + Welcome_Page_Label);
    }

    @Test(description = "Regenerate Violations using Update For New Plate ID", priority = 1)
    @Severity(SeverityLevel.CRITICAL)
    public void Regenerate_Violations_using_Update() {
        WelcomePage_Obj.Select_Violation_Management_Btn();
        SearchInViolationsPage_Obj.Select_Search_Violations_Btn();
        SearchInViolationsPage_Obj.Select_Search_For_Violations_By_Status();
        SearchInViolationsPage_Obj.Select_Status_1_Form_125_created();
        SearchInViolationsPage_Obj.Select_Search_For_Violations_Search_Btn();
        SearchInViolationsPage_Obj.Select_Search_For_Violations_Vio_Data_Btn();
        SearchInViolationsPage_Obj.Select_Closure_Btn();
        SearchInViolationsPage_Obj.Select_Search_For_Violations_Vio_Date_Btn();
        SearchInViolationsPage_Obj.Select_Closure_Btn();
        SearchInViolationsPage_Obj.Select_Search_For_Violations_Edit_Vio_Btn();
        Form125Page_Obj.Wait_Invisibility_Of_Page_Spinner();
    }

    @Test(description = "Regenerate 125 Form for New Plate ID", priority = 2)
    @Severity(SeverityLevel.CRITICAL)
    public void Regenerate_125_Form_using_New_Plate_ID() {
        Form125Page_Obj.Type_Form_125_Plate_ID();
        Form125Page_Obj.Select_Form_125_Type_of_Offense();
        Form125Page_Obj.Select_List_Of_Form_125_Type_Offense_Child();
        Form125Page_Obj.Select_Offenses_List_closer_Btn();
        Form125Page_Obj.Select_Form_125_Creation_Btn();
        Form125Page_Obj.Select_Form_125_Generation_Btn();
        Form125Page_Obj.Wait_Invisibility_Of_Page_Spinner();
        WelcomePage_Obj.Select_Main_Menu_Btn();
    }

}
