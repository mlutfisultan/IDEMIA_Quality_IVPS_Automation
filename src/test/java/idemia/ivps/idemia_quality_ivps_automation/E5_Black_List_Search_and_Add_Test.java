package idemia.ivps.idemia_quality_ivps_automation;

import com.shaft.driver.DriverFactory;
import com.shaft.gui.browser.BrowserActions;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.A1_Main_Page;
import pages.B2_Welcome_Page;
import pages.H8_Black_List_Cars_Page;

public class E5_Black_List_Search_and_Add_Test {
    private WebDriver driver;
    private A1_Main_Page MainPage_Obj;
    private B2_Welcome_Page WelcomePage_Obj;
    private H8_Black_List_Cars_Page BlackListPage_Obj;

    @BeforeClass
    public void setUp() {
        driver = DriverFactory.getDriver();
        driver.get("http://cqcehap001.qcenv.idemia.local:8000/vps-hub/login");
        MainPage_Obj = new A1_Main_Page(driver);
        WelcomePage_Obj = new B2_Welcome_Page(driver);
        BlackListPage_Obj = new H8_Black_List_Cars_Page(driver);
    }

    @AfterClass
    public void tearDown() {
        WelcomePage_Obj.IVPS_Logout();
        BrowserActions.closeCurrentWindow(driver);
    }

    @Test(description = "Login to IVPS", priority = 0)
    @Severity(SeverityLevel.BLOCKER)
    public void IVPS_Login() {
        MainPage_Obj.IVPS_SignIn();
        String Welcome_Page_Label = driver.findElement(WelcomePage_Obj.Welcome_Page_Label).getText();
        System.out.println("Page Label is " + Welcome_Page_Label);
    }

    @Test(description = "Search for Added Plate ID in Black List Previously", priority = 1)
    @Severity(SeverityLevel.MINOR)
    public void Search_for_Black_List_Car_Added_Before() {
        WelcomePage_Obj.Select_Black_List_Cars_Btn();
        BlackListPage_Obj.Type_Black_List_Cars_Search_using_New_Plate_ID();
        BlackListPage_Obj.Select_Black_List_Cars_Search_Plate_ID_Btn();
        BlackListPage_Obj.Get_Black_List_Cars_Search_Plate_Count();
        String Get_Black_List_Cars_Search_Plate_Count = driver.findElement(BlackListPage_Obj.Search_Count).getText();
        System.out.println("Search result = " + Get_Black_List_Cars_Search_Plate_Count);
        BlackListPage_Obj.Select_Black_List_Cars_DisplayAll_Plate_ID_Btn();
    }

    @Test(description = "Add Plate ID Black List", priority = 2)
    @Severity(SeverityLevel.CRITICAL)
    public void Add_Car_in_Black_List() {
        BlackListPage_Obj.Select_Black_List_Cars_New_Plate_Number_Btn();
        BlackListPage_Obj.Type_Black_List_Cars_Add_New_Plate_ID();
        BlackListPage_Obj.Select_Black_List_Cars_Add_Plate_ID_Btn();
        String Get_White_List_Success_Msg = driver.findElement(BlackListPage_Obj.Success_Msg).getText();
        System.out.println("Search result = " + Get_White_List_Success_Msg);
    }

    @Test(description = "Search for Added Plate ID in Black List Recently ", priority = 3)
    @Severity(SeverityLevel.NORMAL)
    public void Search_FOR_Add_BlackListCar() {
        BlackListPage_Obj.Type_Black_List_Cars_Search_using_New_Plate_ID();
        BlackListPage_Obj.Select_Black_List_Cars_Search_Plate_ID_Btn();
        BlackListPage_Obj.Get_Black_List_Cars_Search_Plate_Count();
        String Get_Black_List_Cars_Search_Plate_Count = driver.findElement(BlackListPage_Obj.Search_Count).getText();
        System.out.println("Search result = " + Get_Black_List_Cars_Search_Plate_Count);
        BlackListPage_Obj.Select_Black_List_Cars_DisplayAll_Plate_ID_Btn();
    }
}
