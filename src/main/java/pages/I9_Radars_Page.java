package pages;

import com.shaft.gui.element.ElementActions;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class I9_Radars_Page {
    public By Add_New_Radar_Btn = By.id("ivps-tool-bar-mestamanager-addnew-0");
    public By New_Radar_Name = By.id("add-edit-view-fusion-fusionCode-input");
    public By New_Radar_Maker_List = By.cssSelector("#add-edit-view-fusion-maker-input .p-dropdown");
    public By New_Radar_Maker_Child_1 = By.xpath("//span[text()='IDEMIA']");
    public By New_Radar_Model_List = By.cssSelector("#add-edit-view-fusion-model-input .p-inputtext");
    public By New_Radar_Model_1 = By.cssSelector("p-dropdownitem:nth-child(1) > .p-dropdown-item");
    public By New_Radar_Model_2 = By.cssSelector("p-dropdownitem:nth-child(2) > .p-dropdown-item");
    public By New_Radar_Model_3 = By.cssSelector("p-dropdownitem:nth-child(3) > .p-dropdown-item");
    public By New_Radar_Model_4 = By.cssSelector("p-dropdownitem:nth-child(4) > .p-dropdown-item");
    public By New_Radar_Location = By.id("add-edit-view-fusion-location-input");
    public By New_Radar_Longitude = By.id("add-edit-view-fusion-longitude-input");
    public By New_Radar_Latitude = By.id("add-edit-view-fusion-latitude-input");
    public By New_Radar_Address = By.id("add-edit-view-fusion-ipAddress-input");
    public By Confirmation_To_Add_Btn = By.id("add-edit-view-fusion-confirm-button");
    public By Confirmation_To_Cancel_Btn = By.id("add-edit-view-fusion-cancel-button");
    public By Display_All_Btn = By.id("ivps-tool-bar-mestamanager-showall-1");
    public By Search_By_Radar_Code = By.id("form-control-radarCode-input");
    public By Search_Btn = By.id("dynamic-form-submit-form-button");
    public By Edit_Btn = By.id("mesta-manager-table-edit");
    public By Confirmation_To_Edit_Yes_Btn = By.id("mesta-manager-dialog-yes");
    public By Confirmation_To_Edit_No_Btn = By.id("mesta-manager-dialog-no");
    public By Search_Count = By.cssSelector(".p-datatable-tfoot td:nth-child(2)");
    public By IDEMIA_Spinner = By.cssSelector("i.fa-solid.fa-spinner.label-color.fa-spin.loader-size");

    WebDriver driver;
    String Radar_Name;

    public I9_Radars_Page(WebDriver driver) {
        this.driver = driver;
    }

    public void Wait_Invisibility_Of_Page_Spinner() {
        WebDriverWait Wait = new WebDriverWait(driver, Duration.ofSeconds(50));
        Wait.until(ExpectedConditions.invisibilityOfAllElements(driver.findElements(IDEMIA_Spinner)));
    }

    public void Select_Radar_Add_New_Radar_Btn() {
        ElementActions.click(driver, Add_New_Radar_Btn);
    }

    public void Type_Radar_Add_New_Radar_Code_Name() {
        ElementActions.type(driver, New_Radar_Name, Get_Radar_Name());
    }

    public void Select_Radar_Add_New_Radar_Maker_List() {
        ElementActions.click(driver, New_Radar_Maker_List);
    }

    public void Select_Radar_Add_New_Radar_Maker_IDEMIA() {
        ElementActions.click(driver, New_Radar_Maker_Child_1);
    }

    public void Select_Radar_Add_New_Radar_Model() {
        ElementActions.click(driver, New_Radar_Model_List);
    }

    public void Select_Radar_Add_New_Radar_Model_1() {
        ElementActions.click(driver, New_Radar_Model_1);
    }

    public void Select_Radar_Add_New_Radar_Model_2() {
        ElementActions.click(driver, New_Radar_Model_2);
    }

    public void Select_Radar_Add_New_Radar_Model_3() {
        ElementActions.click(driver, New_Radar_Model_3);
    }

    public void Select_Radar_Add_New_Radar_Model_4() {
        ElementActions.click(driver, New_Radar_Model_4);
    }

    public void Type_Radar_Add_New_Radar_Location() {
        ElementActions.type(driver, New_Radar_Location, "IDEMIA Radar Test");
    }

    public void Type_Radar_Add_New_Radar_Longitude() {
        ElementActions.type(driver, New_Radar_Longitude, "100.100");
    }

    public void Type_Radar_Add_New_Radar_Latitude() {
        ElementActions.type(driver, New_Radar_Latitude, "100.160");
    }

    public void Type_Radar_Add_New_Radar_Address() {
        ElementActions.type(driver, New_Radar_Address, "192.192.192.192");
    }

    public void Select_Radar_Add_New_Radar_Add_Btn() {
        ElementActions.click(driver, Confirmation_To_Add_Btn);
    }

    public void Select_Radar_Add_New_Radar_Cancel_Btn() {
        ElementActions.click(driver, Confirmation_To_Cancel_Btn);
    }

    public void Select_Radar_Add_New_Display_All_Btn() {
        ElementActions.click(driver, Display_All_Btn);
    }

    public void Type_Radar_Add_New_Search_By_Radar_Code() {
        ElementActions.type(driver, Search_By_Radar_Code, Radar_Name);
    }

    public void Select_Radar_Add_New_Search_Btn() {
        ElementActions.click(driver, Search_Btn);
    }

    public void Get_Radar_Add_New_Search_Count() {
        ElementActions.getElementsCount(driver, Search_Count);
    }

    public void Select_Edit_Data_Btn() {
        ElementActions.click(driver, Edit_Btn);
    }

    public void Select_Confirmation_To_Edit_Yes_Btn() {
        ElementActions.click(driver, Confirmation_To_Edit_Yes_Btn);
    }

    public void Select_Confirmation_To_Edit_No_Btn() {
        ElementActions.click(driver, Confirmation_To_Edit_No_Btn);
    }

    public String Get_Radar_Name() {
        Radar_Name = RandomStringUtils.randomAlphanumeric(8);
        return Radar_Name;
    }
}
