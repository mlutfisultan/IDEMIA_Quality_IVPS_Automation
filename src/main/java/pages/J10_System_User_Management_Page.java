package pages;

import com.shaft.gui.element.ElementActions;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class J10_System_User_Management_Page {
    public By User_Manage_Add_New_User_Btn = By.id("ivps-tool-bar-usersmanagement-addnew-0");
    public By User_Manage_New_User_Full_Name = By.id("add-edit-user-fullname-input");
    public By User_Manage_New_User_Permission_List = By.cssSelector("#add-edit-user-privilege-input .p-multiselect");
    public By User_Manage_New_User_Permission_All = By.cssSelector(".p-checkbox-box:nth-child(2)");
    public By User_Manage_New_User_Permission_1 = By.cssSelector("p-multiselectitem:nth-child(1)  > .p-multiselect-item");
    public By User_Manage_New_User_Permission_2 = By.cssSelector("p-multiselectitem:nth-child(2)  > .p-multiselect-item");
    public By User_Manage_New_User_Permission_3 = By.cssSelector("p-multiselectitem:nth-child(3)  > .p-multiselect-item");
    public By User_Manage_New_User_Permission_4 = By.cssSelector("p-multiselectitem:nth-child(4)  > .p-multiselect-item");
    public By User_Manage_New_User_Permission_5 = By.cssSelector("p-multiselectitem:nth-child(5)  > .p-multiselect-item");
    public By User_Manage_New_User_Name = By.id("add-edit-user-username-input");
    public By User_Manage_New_Password = By.id("add-edit-user-password-input");
    public By User_Manage_New_User_Add_Btn = By.id("add-edit-user-confirm-button");
    public By User_Manage_New_Cancel_Add_Btn = By.id("add-edit-user-cancel-button");
    public By User_Manage_Display_All_Btn = By.id("ivps-tool-bar-usersmanagement-showall-1");
    public By User_Manage_Search_by_Username = By.id("form-control-userName-input");
    public By User_Manage_Search_Btn = By.id("dynamic-form-submit-form-button");
    public By User_Manage_Search_Count = By.cssSelector(".p-datatable-tfoot td:nth-child(2)");
    public By User_Manage_Back_Btn = By.id("breadcrumb-back-button");
    public By IDEMIA_Spinner = By.cssSelector("i.fa-solid.fa-spinner.label-color.fa-spin.loader-size");


    WebDriver driver;
    String User_Name;

    public J10_System_User_Management_Page(WebDriver driver) {
        this.driver = driver;
    }

    public void Wait_Invisibility_Of_Page_Spinner() {
        WebDriverWait Wait = new WebDriverWait(driver, Duration.ofSeconds(50));
        Wait.until(ExpectedConditions.invisibilityOfAllElements(driver.findElements(IDEMIA_Spinner)));
    }

    public void Select_User_Manage_Add_New_User_Btn() {
        ElementActions.click(driver, User_Manage_Add_New_User_Btn);
    }

    public void Type_User_Manage_New_User_Full_Name() {
        ElementActions.type(driver, User_Manage_New_User_Full_Name, Get_User_Name());
    }

    public void Select_User_Manage_New_User_Permission_List() {
        ElementActions.click(driver, User_Manage_New_User_Permission_List);
    }

    public void Select_User_Manage_New_User_Permission_All() {
        ElementActions.click(driver, User_Manage_New_User_Permission_All);
    }

    public void Select_User_Manage_New_User_Permission_1() {
        ElementActions.click(driver, User_Manage_New_User_Permission_1);
    }

    public void Select_User_Manage_New_User_Permission_2() {
        ElementActions.click(driver, User_Manage_New_User_Permission_2);
    }

    public void Select_User_Manage_New_User_Permission_3() {
        ElementActions.click(driver, User_Manage_New_User_Permission_3);
    }

    public void Select_User_Manage_New_User_Permission_4() {
        ElementActions.click(driver, User_Manage_New_User_Permission_4);
    }

    public void Select_User_Manage_New_User_Permission_5() {
        ElementActions.click(driver, User_Manage_New_User_Permission_5);
    }

    public void Select_All_User_Manage_New_User_Permissions() {
        ElementActions.click(driver, User_Manage_New_User_Permission_1);
        ElementActions.click(driver, User_Manage_New_User_Permission_2);
        ElementActions.click(driver, User_Manage_New_User_Permission_3);
        ElementActions.click(driver, User_Manage_New_User_Permission_4);
        ElementActions.click(driver, User_Manage_New_User_Permission_5);
    }

    public void Type_User_Manage_New_User_Name() {
        ElementActions.type(driver, User_Manage_New_User_Name, Get_User_Name());
    }

    public void Type_User_Manage_New_Password() {
        ElementActions.type(driver, User_Manage_New_Password, User_Name);
    }

    public void Select_User_Manage_New_User_Add_Btn() {
        ElementActions.click(driver, User_Manage_New_User_Add_Btn);
    }

    public void Select_User_Manage_New_User_Cancel_Btn() {
        ElementActions.click(driver, User_Manage_New_Cancel_Add_Btn);
    }

    public void Select_User_Manage_Display_All_Btn() {
        ElementActions.click(driver, User_Manage_Display_All_Btn);
    }

    public void Type_User_Manage_Search_by_Username() {
        ElementActions.type(driver, User_Manage_Search_by_Username, User_Name);
    }

    public void Select_User_Manage_Search_Btn() {
        ElementActions.click(driver, User_Manage_Search_Btn);
    }


    public void Get_User_Manage_Search_Count() {
        ElementActions.getElementsCount(driver, User_Manage_Search_Count);
    }

    public void Select_User_Manage_Back_Btn() {
        ElementActions.click(driver, User_Manage_Back_Btn);
    }

    public String Get_User_Name() {
        User_Name = RandomStringUtils.randomAlphabetic(10);
        return User_Name;
    }
}
