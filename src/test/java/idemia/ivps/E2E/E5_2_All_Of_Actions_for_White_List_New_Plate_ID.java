package idemia.ivps.E2E;

import com.shaft.driver.DriverFactory;
import com.shaft.gui.browser.BrowserActions;
import com.shaft.gui.element.ElementActions;
import com.shaft.validation.Assertions;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.*;

public class E5_2_All_Of_Actions_for_White_List_New_Plate_ID {
    private WebDriver driver;
    private A1_Main_Page MainPage_Obj;
    private B2_Welcome_Page WelcomePage_Obj;
    private C3_Form_125_Page Form125Page_Obj;
    private D4_Search_In_Violations_Page SearchInViolationsPage_Obj;
    private G7_White_List_Cars_Page WhiteListPage_Obj;

    @BeforeClass
    public void setUp() {
        driver = DriverFactory.getDriver();
        driver.get("http://cqcehap001.qcenv.idemia.local:8000/vps-hub/login");
        MainPage_Obj = new A1_Main_Page(driver);
        WelcomePage_Obj = new B2_Welcome_Page(driver);
        Form125Page_Obj = new C3_Form_125_Page(driver);
        SearchInViolationsPage_Obj = new D4_Search_In_Violations_Page(driver);
        WhiteListPage_Obj = new G7_White_List_Cars_Page(driver);

    }

    @AfterClass
    public void tearDown() {
        WelcomePage_Obj.IVPS_Logout();
        BrowserActions.closeCurrentWindow(driver);
    }

    @Test(description = "Login to IVPS", priority = 0)
    @Severity(SeverityLevel.BLOCKER)
    public void IVPS_Login() {
        MainPage_Obj.IVPS_SignIn();
        String Welcome_Page_Label = driver.findElement(WelcomePage_Obj.Welcome_Page_Label).getText();
        System.out.println("Page Label is " + Welcome_Page_Label);
    }

    @Test(description = "Add and Search for New Plate ID White List", priority = 1)
    @Severity(SeverityLevel.CRITICAL)
    public void Add_and_Search_New_Plate_ID_in_White_List() {
        WelcomePage_Obj.Select_White_List_Cars_Btn();
        WhiteListPage_Obj.Select_White_List_New_Plate_Number_Btn();
        WhiteListPage_Obj.Type_White_List_Add_New_Plate_ID();
        WhiteListPage_Obj.Select_White_List_Add_Plate_ID_Btn();
        WhiteListPage_Obj.Type_White_List_Search_using_New_Plate_ID();
        WhiteListPage_Obj.Select_White_List_Search_Plate_ID_Btn();
        WhiteListPage_Obj.Get_White_List_Search_Plate_Count();
        String Get_White_List_Search_Plate_Count = driver.findElement(WhiteListPage_Obj.Search_Plate_Count).getText();
        System.out.println("Search result = " + Get_White_List_Search_Plate_Count);
    }

    @Test(description = "Create and Submit 125 Form for White List New Plate ID", priority = 2)
    @Severity(SeverityLevel.CRITICAL)
    public void Create_and_Submit_125_Form_using_White_List_New_Plate_ID() {
        WelcomePage_Obj.Select_Main_Menu_Btn();
        WelcomePage_Obj.Select_Violation_Management_Btn();
        Form125Page_Obj.Select_Form_125_Generator_Btn();
        Form125Page_Obj.Select_Form_125_Arrangement_Type();
        Form125Page_Obj.Select_Form_125_Arrangement_Type_Dsc();
        Form125Page_Obj.Type_Form_125_Violation_From_Date();
        Form125Page_Obj.Select_Form_125_Review_Activation();
        Form125Page_Obj.Select_Form_125_Review_Activation_Yes();
        Form125Page_Obj.Select_Form_125_View_Violations_Btn();
        Form125Page_Obj.Type_Form_125_Plate_ID();
        Form125Page_Obj.Select_Form_125_Type_of_Offense();
        Form125Page_Obj.Select_List_Of_Form_125_Type_Offense_Child();
        Form125Page_Obj.Select_Offenses_List_closer_Btn();
        Form125Page_Obj.Select_Form_125_Creation_Btn();
        Form125Page_Obj.Wait_Invisibility_Of_Page_Spinner();
        WelcomePage_Obj.Select_Main_Menu_Btn();
    }

    @Test(description = "Search of Violations for White List New Plate ID", priority = 3)
    @Severity(SeverityLevel.NORMAL)
    public void Search_For_Violations_With_New_Plate() {
        WelcomePage_Obj.Select_Main_Menu_Btn();
        WelcomePage_Obj.Select_Violation_Management_Btn();
        SearchInViolationsPage_Obj.Select_Search_Violations_Btn();
        SearchInViolationsPage_Obj.Type_Search_For_Violations_Plate_Number_New();
        SearchInViolationsPage_Obj.Select_Search_For_Violations_Search_Btn();
        String Get_White_List_Vio_Status = driver.findElement(WhiteListPage_Obj.Vio_Status).getText();
        System.out.println("Search result = " + Get_White_List_Vio_Status);
        ElementActions.getElementsCount(driver, SearchInViolationsPage_Obj.Search_Result);
        String Search_For_Violations_Search_Result = driver.findElement(SearchInViolationsPage_Obj.Search_Result).getText();
        System.out.println("Search result = " + Search_For_Violations_Search_Result);
    }

    @Test(description = "Search and Deactivate for New Plate ID White List", priority = 4)
    @Severity(SeverityLevel.CRITICAL)
    public void Search_and_Deactivate_New_Plate_ID_in_White_List() {
        WelcomePage_Obj.Select_Main_Menu_Btn();
        WelcomePage_Obj.Select_White_List_Cars_Btn();
        WhiteListPage_Obj.Type_White_List_Search_using_New_Plate_ID();
        WhiteListPage_Obj.Select_White_List_Search_Plate_ID_Btn();
        WhiteListPage_Obj.Get_White_List_Search_Plate_Count();
        String Get_White_List_Search_Plate_Count = driver.findElement(WhiteListPage_Obj.Search_Plate_Count).getText();
        System.out.println("Search result = " + Get_White_List_Search_Plate_Count);
        WhiteListPage_Obj.Select_White_List_Active_Deactivate_Btn();
        WhiteListPage_Obj.Select_White_List_Confirmation_Yes_Btn();
        String Get_White_List_Success_Msg = driver.findElement(WhiteListPage_Obj.Success_Msg).getText();
        System.out.println("Search result = " + Get_White_List_Success_Msg);
    }

    @Test(description = "Create and Submit 125 Form for Deactivated White List New Plate ID", priority = 5)
    @Severity(SeverityLevel.CRITICAL)
    public void Create_and_Submit_125_Form_using_Deactivated_White_List_New_Plate_ID() {
        WelcomePage_Obj.Select_Main_Menu_Btn();
        WelcomePage_Obj.Select_Violation_Management_Btn();
        Form125Page_Obj.Select_Form_125_Generator_Btn();
        Form125Page_Obj.Select_Form_125_Arrangement_Type();
        Form125Page_Obj.Select_Form_125_Arrangement_Type_Dsc();
        Form125Page_Obj.Type_Form_125_Violation_From_Date();
        Form125Page_Obj.Select_Form_125_Review_Activation();
        Form125Page_Obj.Select_Form_125_Review_Activation_Yes();
        Form125Page_Obj.Select_Form_125_View_Violations_Btn();
        Form125Page_Obj.Type_Form_125_Plate_ID();
        Form125Page_Obj.Select_Form_125_Type_of_Offense();
        Form125Page_Obj.Select_List_Of_Form_125_Type_Offense_Child();
        Form125Page_Obj.Select_Offenses_List_closer_Btn();
        Form125Page_Obj.Select_Form_125_Creation_Btn();
        Form125Page_Obj.Select_Form_125_Generation_Btn();
        Form125Page_Obj.Wait_Invisibility_Of_Page_Spinner();
        WelcomePage_Obj.Select_Main_Menu_Btn();
    }

    @Test(description = "Search of Violations for Deactivated White List New Plate ID", priority = 6)
    @Severity(SeverityLevel.NORMAL)
    public void Search_For_Violations_With_Deactivated_New_Plate_ID() {
        WelcomePage_Obj.Select_Main_Menu_Btn();
        WelcomePage_Obj.Select_Violation_Management_Btn();
        SearchInViolationsPage_Obj.Select_Search_Violations_Btn();
        SearchInViolationsPage_Obj.Type_Search_For_Violations_Plate_Number_New();
        SearchInViolationsPage_Obj.Select_Search_For_Violations_Search_Btn();
        String Get_White_List_Vio_Status = driver.findElement(WhiteListPage_Obj.Vio_Status).getText();
        System.out.println("Search result = " + Get_White_List_Vio_Status);
        Assertions.assertEquals(driver.findElement(WhiteListPage_Obj.Vio_Status), driver.findElement(WhiteListPage_Obj.Vio_Status), "تمت تكوين نموذج 125");
        ElementActions.getElementsCount(driver, SearchInViolationsPage_Obj.Search_Result);
        String Search_For_Violations_Search_Result = driver.findElement(SearchInViolationsPage_Obj.Search_Result).getText();
        System.out.println("Search result = " + Search_For_Violations_Search_Result);
    }
}
