package pages;

import com.shaft.gui.element.ElementActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class G7_White_List_Cars_Page {
    public By Add_New_Plate_ID_Btn = By.id("ivps-tool-bar-whitelistmanager-addnew-0");
    public By Plate_ID_1 = By.id("cpn1");
    public By Plate_ID_2 = By.id("cpn2");
    public By Plate_ID_3 = By.id("cpn3");
    public By Plate_ID_4 = By.id("cpn4");
    public By Plate_ID_5 = By.id("cpn5");
    public By Confirmation_To_Add_Btn = By.id("add-license-plate-confirm-button");
    public By Confirmation_To_Cancel_Btn = By.id("add-license-plate-cancel-button");
    public By Display_All_Btn = By.id("ivps-tool-bar-whitelistmanager-showall-1");
    public By Search_By_Plate_ID = By.id("form-control-plateNumber-input");
    public By Search_Btn = By.id("dynamic-form-submit-form-button");
    public By Search_Plate_Count = By.cssSelector(".p-datatable-tfoot td:nth-child(2)");
    public By Active_Deactivate_Btn = By.id("whitelist-manager-table-activate-button-0");
    public By Confirmation_Yes_Btn = By.id("whitelist-manager-dialog-yes");
    public By Confirmation_No_Btn = By.id("whitelist-manager-dialog-no");
    public By Success_Msg = By.cssSelector(".toast-message");
    public By Vio_Status = By.cssSelector("td:nth-child(7)");
    WebDriver driver;

    public G7_White_List_Cars_Page(WebDriver driver) {
        this.driver = driver;
    }

    public void Select_White_List_New_Plate_Number_Btn() {
        ElementActions.click(driver, Add_New_Plate_ID_Btn);
    }

    public void Type_White_List_Add_New_Plate_ID() {
        ElementActions.type(driver, Plate_ID_1, O15_Plate_Number_Generator.getPlateNumberByIndex(0));
        ElementActions.type(driver, Plate_ID_2, O15_Plate_Number_Generator.getPlateNumberByIndex(1));
        ElementActions.type(driver, Plate_ID_3, O15_Plate_Number_Generator.getPlateNumberByIndex(2));
//        ElementActions.type(driver, Form_125_Plate_ID_4, getRandomCharacter());
        ElementActions.type(driver, Plate_ID_5, O15_Plate_Number_Generator.getPlateNumberByIndex(3));
    }

    public void Select_White_List_Add_Plate_ID_Btn() {
        ElementActions.click(driver, Confirmation_To_Add_Btn);
    }

    public void Select_White_List_Cancel_Plate_ID_Btn() {
        ElementActions.click(driver, Confirmation_To_Cancel_Btn);
    }

    public void Select_White_List_DisplayAll_Plate_ID_Btn() {
        ElementActions.click(driver, Display_All_Btn);
    }

    public void Type_White_List_Search_using_New_Plate_ID() {
        ElementActions.type(driver, Search_By_Plate_ID, O15_Plate_Number_Generator.getNewPlateNumber());
    }

    public void Select_White_List_Search_Plate_ID_Btn() {
        ElementActions.click(driver, Search_Btn);
    }

    public void Select_White_List_Active_Deactivate_Btn() {
        ElementActions.click(driver, Active_Deactivate_Btn);
    }

    public void Get_White_List_Search_Plate_Count() {
        ElementActions.getElementsCount(driver, Search_Plate_Count);
    }

    public void Select_White_List_Confirmation_Yes_Btn() {
        ElementActions.click(driver, Confirmation_Yes_Btn);
    }

    public void SSelect_White_List_Confirmation_No_Btn() {
        ElementActions.click(driver, Confirmation_No_Btn);
    }
}
